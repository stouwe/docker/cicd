FROM alpine:latest

RUN apk update && \
  set -ex && \
  apk add ca-certificates && update-ca-certificates && \
  apk add --no-cache --update \
  bash \
  curl \
  findutils \
  git \
  jq \
  tzdata \
  unzip \
  sed \
  py3-pip \
  npm \
  openssl

#RUN apk --update add --virtual .build-dependencies \
#  build-base \
#  libffi-dev \
#  libxml2-dev \
#  libxslt-dev \
#  py3-pip

RUN pip3 install --no-cache --upgrade \
  awscli \
#  awsebcli \
  boto3 \
  requests

RUN rm -rf /var/cache/apk/* /tmp/*

RUN git clone https://github.com/tfutils/tfenv.git ~/.tfenv && \
  ln -s ~/.tfenv/bin/* /usr/local/bin && \
  tfenv install 1.0.2 && \
  echo '1.0.2' > ~/.tfenv/version

RUN npm install serverless -g --silent
RUN wget -O /usr/local/bin/kubectl "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
 wget -O /usr/local/bin/kops https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64 && \
 wget -O /usr/local/bin/helmfile "https://github.com/roboll/helmfile/releases/download/$(curl -s https://api.github.com/repos/roboll/helmfile/releases/latest | grep tag_name | cut -d '"' -f 4)/helmfile_linux_amd64" \
 && chmod a+x /usr/local/bin/kops /usr/local/bin/kubectl /usr/local/bin/helmfile

RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
RUN helm plugin install https://github.com/databus23/helm-diff

COPY --from=vault:1.6.1 /bin/vault /bin/vault
COPY --from=registry.gitlab.com/gitlab-org/terraform-images/stable:latest /usr/bin/gitlab-terraform /usr/bin/gitlab-terraform
ENTRYPOINT []
CMD ["/bin/bash"]
